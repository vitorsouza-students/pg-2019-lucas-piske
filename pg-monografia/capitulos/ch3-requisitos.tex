% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 3 - Especificação de Requisitos
% ==============================================================================
\chapter{Especificação de Requisitos}
\label{sec-requisitos}

Neste capítulo são apresentados alguns resultados do processo de Engenharia de Requisitos realizado durante a criação deste sistema. Na Seção 3.1 é descrito o escopo do projeto; na Seção 3.2 são apresentados os diagramas dos casos de uso identificados e na Seção 3.3 são discutidos os requisitos não-funcionais mais relevantes para o desenvolvimento do sistema.

\section{Descrição do Escopo}
\label{sec-des-escopo}
Atualmente praticamente todos os sistemas de informações desenvolvidos precisam de uma forma de salvar dados. Essa necessidade pode aparecer durante o projeto de um sistema por diferentes motivos, como por exemplo, as leis de um país podem estabelecer que as ações de seus usuários devam ser arquivadas para que no futuro caso seja necessário elas possam ser auditadas por algum orgão do governo.

Para suprir esta necessidade é comum que estes sistemas de informação usem sistemas de gerenciamento de banco de dados de prateleira, como MySQL ou PostgreSQL. Isso acontece pois implementar persistência e recuperação de dados poderia levar muito tempo e iria requerer pessoas com muita experiência nesta área da Computação para desenvolver uma solução satisfatória, como a oferecida por produtos de prateleira já existentes. 

Muitos tipos destes dados que os sistemas atuais precisam salvar e recuperar necessitam de uma forma de serem referenciados de forma única e que a informação usada para referenciá-los se mantenha a mesma. Mas para muitos destes tipos de dados não existe um atributo natural que satisfaça estes requisitos, um exemplo, seria um usuário em uma plataforma social, já que suas informações podem a qualquer momento mudar, como nome de usuário, email e etc, não existe nenhuma informação deste usuário que pode ser usada para referenciá-lo.

Portanto, para que seja possível referenciar estes tipos de dados, são gerados identificadores artificiais que são adicionados junto a estes registros e, no futuro, caso seja necessário referenciá-los, este identificador artificial pode ser usado. Uma estratégia comum para gerar estes identificadores artificiais é guardar um número inteiro que é atribuído a cada novo registro e depois incrementado, assim impedindo que dois registros possuam o mesmo identificador e estes registros nunca precisarão modificar os valores destes identificadores.

Estes identificadores podem ser gerados pelos próprios gerenciadores de banco de dados, mas quando uma aplicação necessita de um \textit{cluster} de banco de dados com múltiplos nós que aceitem operações de escrita, de forma concorrente, e seguindo uma arquitetura de particionamento horizontal, esta operação de geração de identificadores, da forma que é implementada geralmente pelos gerenciadores de banco de dados, não consegue ser escalada assim como as escritas de registros nesta topologia proposta.

Isso acontece porque os gerenciadores de banco de dados implementam a geração de identificadores usando números inteiros que são incrementados a cada vez que o último valor é usado. Para isso somente uma instância do \textit{cluster} do banco de dados pode manter o último valor usado e todas as outras teriam que comunicar com esta instância toda vez que fosse necessário gerar um identificador. Criando um único ponto de falha e um gargalo de performance para todas as operações que necessitem gerar um identificador.

O sistema Sandgrain será criado para permitir a geração de identificadores 64-bits de forma simples 
para quando desenvolvedores precisam da topologia de sistemas de gerenciamento de banco de dados citada anteriormente, mas seus sistemas não podem sofrer com os problemas gerados pelas soluções comuns atuais. Os usuários do sistema Sandgrain poderão ignorar os métodos de geração de identificadores do sistema de banco de dados e utilizar somente o Sandgrain.

As aplicações dos usuários que se integrarem com Sandgrain poderão gerar um conjunto de identificadores fornecendo o nome da sequência a qual esses identificadores pertencem e o nome do \textit{schema} ao qual esta sequência pertence. Neste contexto, \textit{schemas} são recipientes lógicos utilizados para evitar colisões de nomes de sequências. 

Usuários devem criar o \textit{schema} anterior ao seus uso, do contrário a geração de identificadores resultará em erro. \textit{Schemas} poderão ser criados passando somente os seus nomes, sendo que os nomes devem ser únicos. \textit{Schemas} atualmente existentes podem também serem listados e excluídos. 

Usuários também têm a possibilidade de gerenciar as permissões que os outros usuários possuem, podendo escolher quais deles podem efetuar operações específicas em recursos designados. Um exemplo seria permitir que somente o usuário \textbf{usuario\_42} possa utilizar sequências contidas no \textit{schema} \textbf{produtos} para gerar identificadores.

Os operadores do sistemas terão a capacidade de recuperar as métricas do sistema enquanto este está funcionando, para que desta forma possam monitorar a saúde do sistema e, caso o sistema se mostre em más condições, medidas poderão ser tomadas para fazer com que este volte para um estado estável.

\section{Casos de Uso}

O modelo de casos de uso tem como propósito capturar e descrever as funcionalidades que um
sistema deve prover para os atores que interagem com o mesmo. Os atores que foram identificados para este sistema são descritos na Tabela~\ref{table:7}. Os casos de uso de cada ator são exibidos na Figura~\ref{fig-casos-uso-completo}.

\begin{table}[!htb]
	\centering
	\begin{tabular}{|l|l|}
		\hline
		\textbf{Ator}     & \textbf{Descrição}                                                                                                                                                                                  \\ \hline
		Aplicação Cliente & \begin{tabular}[c]{@{}l@{}}Uma aplicação integrada com o sistema Sandgrain \\com a capacidade de gerar identificadores, criar schemas, \\gerenciar permissões e etc.\end{tabular}                            \\ \hline
		Operador       & \begin{tabular}[c]{@{}l@{}}Um usuário ou aplicação com a responsabilidade\\de gerenciar uma implantação do sistema Sandgrain\end{tabular} \\ \hline
	\end{tabular}
	\caption{Atores}
	\label{table:7}
\end{table}

\begin{figure}
\centering
\includegraphics[width=.5\textwidth]{figuras/casos-uso-completo.png} 
\caption{Diagrama de Casos de Uso do sistema Sandgrain}
\label{fig-casos-uso-completo}
\end{figure}

\textbf{Aplicações Cliente} ao se integrarem com Sandgrain tem a capacidade de \textbf{Gerar Identificadores}, \textbf{Gerenciar Schemas}, \textbf{Gerenciar Estado do Cluster} e \textbf{Gerenciar Permissões de Usuários}. Mas a capacidade de uma instância deste tipo de ator
realizar uma destas ações pode ser restringida caso o mesmo não possua a permissão necessária.

Ao usar Sandgrain para \textbf{Gerar Identificadores} a \textbf{Aplicações Cliente} deve fornecer o nome da sequência utilizada para gerar o identificador, o \textit{schema} a qual essa sequência deve pertencer e quantos identificadores devem ser gerados. Como resultado desta operação serão retornados a \textbf{Aplicações Cliente} um conjunto de identificadores.

Como citado na Seção~\ref{sec-des-escopo}, um \textit{schema} deve ser criado no sistema antes que este possa ser usado para gerar identificadores. Estes podem ser criados por uma \textbf{Aplicação Cliente} fornecendo um nome para o novo \textit{schema} a ser criado e este nome deve ser único entre todos os \textit{schemas} criados em uma implantação do sistema. Além de criar \textit{schemas}, \textbf{Aplicação Cliente} também pode listá-los e remove-los.

\textbf{Aplicações Cliente} podem dar ou restringir permissões a outras instâncias deste tipo de ator para que possam realizar ações sobre recursos do sistema, como por exemplo gerar um identificador usando uma dada sequência. Somente \textbf{Aplicações Cliente} que possuem permissão podem editar as dos outros usuários e, quando o sistema é implantado, é configurado um usuário \textit{super} que tem permissão para efetuar todas as operações possíveis no sistema.

\textbf{Operadores} são seres humanos ou programas de computador que possuem a capacidade de receber métricas sobre o estado de funcionamento do sistema a qualquer momento de sua execução sem influenciar o seu funcionamento. Assim, o operador pode monitorar o funcionamento do Sandgrain, identificar possíveis problemas que possam estar acontecendo e tomar medidas para voltar o sistema para um estado normal.

\section{Requisitos Não-Funcionais}
\label{sec:req-nfunc}

Nas seguintes subseções serão discutidos os requisitos não-funcionais que foram considerados como os que terão maior influência sobre o processo de desenvolvimento. A ordem das subseções não possui relação com a ordem de importância dos requisitos listados.

\subsection{Baixa Latência}
Com a geração de identificadores sendo uma operação extremamente essencial e sendo bastante provável que uma aplicação cliente precise de executar a operação de geração de identificadores em uma mesma transação em um pequeno intervalo de tempo ficou claro que é de muita importância para os usuários que o intervalo entre a estimulação para o início da operação e a sua resposta deve ser muito pequena, para que os usuários deste sistema não percam desempenho em suas aplicações. No contexto deste trabalho foi considerada uma latência como baixa se esta possui o 99º inferior a 6 milissegundos quando o sistema e a rede em que se encontra estão em estado normal.



\subsection{Alta Disponibilidade} \label{alta-disp}
Um dos casos de uso mais importantes para os identificadores gerados por esse sistema é a possibilidade de atribuí-los a registros para distinguir um registro do outro. Esta atribuição acontece geralmente quando um registro é criado e caso essa atribuição não seja possível a criação irá falhar. 

Como criar novos registros em um grande número de sistemas é uma operação muito importante, se mostra essencial que este sistema seja capaz de continuar realizando as suas operações, principalmente realizar a geração de identificadores, mesmo que aconteçam falhas de \textit{hardware}, \textit{software} ou energia.

\subsection{Escalabilidade}
\label{reqesca}

Como citado na Subseção~\ref{alta-disp}, um dos casos de uso mais importantes requer que o sistema seja capaz de gerar identificadores ou, do contrário, a aplicação cliente não será capaz de gerar identificadores para atribuí-los a registros de banco de dados e assim distingui-los um dos outros. 

Uma razão para que o sistema não consiga gerar identificadores, além do motivo discutido na Subseção~\ref{alta-disp}, é a taxa de requisições para criação de identificadores por tempo aumentar de forma que uma máquina não seja capaz de lidar com esta carga.

Isso pode acontecer, por exemplo, caso uma notícia que fale sobre a sua aplicação \textit{web} seja publicada e a sua quantidade de usuários cresça muito e rapidamente por causa desta notícia. Se o seu sistema não estiver preparado para lidar com este crescimento a qualidade do serviço muito provavelmente irá se degradar, já que os recursos necessários para lidar com estes novos usuários será bem maior que anteriormente.

Por esta razão se tornou de suma importância que o sistema seja apto a lidar com um aumento de trabalho a ser realizado simplesmente aumentando a quantidade de recursos de processamento e memória disponíveis. Isso pode ser feito melhorando o \textit{hardware} da máquina executando o sistema ou usando diversas máquinas trabalhando em conjunto.  

\subsection{Segurança}
A fim de prover segurança para seus usuários o sistema irá fornecer uma forma de exigir que todas as operações possíveis de serem feitas por \textbf{Aplicações Clientes} sejam autenticadas, ou seja, deverá ser possível saber quem está executando dada operação.

Além disso, usuários serão capazes de especificar quais usuários são permitidos ou não a executarem certas operações. Um exemplo seria não permitir proibir um dado usuário de gerar identificadores usando uma sequência específica ou permitir um usuário a criar \textit{schemas}.

Por fim também será oferecido ao usuário a possibilidade de proteger os dados enviados e recebidos do sistema Sandgrain de possíveis atacantes que tentem visualizar estas informações sem permissão ou modificá-las.