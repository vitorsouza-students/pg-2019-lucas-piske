% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 3 - Projeto Arquitetural e Implementação
% ==============================================================================
\chapter{Projeto Arquitetural e Implementação}
\label{sec-projeto}
Após a fase de especificação e análise de requisitos, foi realizado o projeto do sistema. Nesta fase é realizada a modelagem da estrutura geral deste projeto e como estas estruturas interagem umas com as outras. Tais estruturas podem ser diferentes dependendo da perspectiva que se esteja analisando o sistema, como uma perspectiva física por exemplo.

O projeto e implementação de software é um estágio do processo no qual um sistema de software executável é desenvolvido. O projeto de software é uma atividade criativa em que se identificam os componentes de software e seus relacionamentos com base nos requisitos do cliente~\cite{engsoft}.

Este capítulo discute de forma breve o Projeto Arquitetural e a Implementação
do sistema Sandgrain. Na Seção~\ref{sec:visaogeralsol} é discutida a solução do problema de geração de identificadores por uma visão geral, na Seção~\ref{sec:tecusadas} são descritas as tecnologias usadas para a implementação deste projeto, na Seção~\ref{sec:arq-fis}, a arquitetura do sistema é analisada de uma perspectiva física, nas seções~\ref{arqserv} e~\ref{sec:bib-client} são discutidas as implementações do servidor Sandgrain e da biblioteca usada pelos clientes para comunicar com o servidor, respectivamente.

\section{Visão Geral da Solução}
\label{sec:visaogeralsol}

A fim de facilitar o entendimento da arquitetura e implementação deste projeto, esta seção discute a solução elaborada para a geração dos identificadores e todos os conceitos essenciais para o seu entendimento, criando assim uma base para a compreensão das responsabilidades dos diferentes componentes deste sistema.

\subsection{Geração de Identificadores}

Com objetivo de gerar identificadores, enquanto respeitando os requisitos não-funcionais, o sistema deve ser composto de um \textit{cluster} de instâncias que consigam se coordenar, para realizar esta tarefa, mas também devem ser pouco dependente umas nas outras, de forma que, por exemplo, caso uma instância pare de funcionar e seja encerrada, isso não afete o funcionamento das outras instâncias.

A coordenação entre as instâncias no \textit{cluster}, neste projeto, é feita usando o serviço distribuído ZooKeeper, explicado em mais detalhes na Seção~\ref{sec:tecusadas}. A autonomia necessária, citada anteriormente, é uma característica que se dá pelo método usado neste projeto para gerar os identificadores.

O identificadores possuem 64-bits por múltiplos motivos, o primeiro é o fato de ser comum que sistemas usem banco de dados para gerarem seus identificadores inicialmente e estes geralmente são inteiros de 64-bits, portanto manter o tipo e o tamanho facilita na transição entre estratégias de geração de identificadores.

A segunda é que um tamanho maior necessitaria de mais espaço de memória ou disco para o seu armazenamento e, para casos nos quais é necessário armazenar um número muito grande destes identificadores, um aumento mesmo de poucos bits pode fazer uma grande diferença. 

A terceira é que um tamanho menor prejudicaria a capacidade de geração de identificadores por milissegundo que o sistema tem, pois diminuiria o número de possíveis valores para um identificador. 

Ao gerar os identificadores 64-bits estes são montados a partir de múltiplos segmentos de bits, cada um com um propósito diferente. O diagrama com estes segmentos representados pode ser visto a seguir:   

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/id-bit-seg-diagram} 
	\caption{Diagrama de Segmentos dos Identificadores}
	\label{fig-id-seg-diagram}
\end{figure}

Os segmentos \textbf{DC Namespace} e \textbf{Instance Namespace} são os responsáveis por prover ao sistema a autonomia discutida. Garantindo que as instâncias de um \textit{cluster} não compartilhem \textbf{Instance Namespaces} estas podem gerar identificadores sem a necessidade de comunicação, pois não haverá a possibilidade de dois identificadores iguais em instâncias diferentes serem gerados.

Removendo a necessidade de comunicação entre as instâncias durante o processo de geração de identificadores faz com que caso uma instância tenha problemas, as outras possam continuar gerando identificadores sem problemas. 

\textbf{DC Namespace} tem a função similar à \textbf{Instance Namespace}, mas é usado em casos que existem múltiplos \textit{clusters}, em diferentes \textit{data centers} por exemplo. Assegurando que cada \textit{cluster} tenha um valor único para \textbf {DC Namespace} estes \textit{clusters} podem gerar identificadores sem se comunicarem, mas também sem a possibilidade de gerarem identificadores duplicados.

O segmento \textbf{Timestamp} é a quantidade de milissegundos decorridos desde de primeiro de janeiro de 1970 até o momento em que o identificador foi gerado. \textbf{Sequence} é um valor inteiro que é incrementado a cada geração que ocorre no mesmo instante de tempo, ou seja, que tenha o mesmo valor para \textbf{Timestamp}.

\textbf{Sign} é o segmento de bits referente ao sinal do número. Neste método este bit é mantido constante, fazendo que os números gerados sempre sejam positivos. 

\subsection{Sequências e Schemas}
Com intuito de aumentar a quantidade de identificadores que podem ser gerados em único milissegundo, foi introduzido o conceito de \textbf{Sequências} ao sistema. Uma \textbf{Sequência} é uma \textit{string} de caracteres que é passada como argumento para geração de identificadores. 

Deve-se garantir que um conjunto de identificadores gerados usando a mesma \textbf{Sequência} não conterá dois elementos iguais. Consequentemente, no caso que um conjunto de identificadores seja criado usando \textbf{Sequências} diferentes, este naturalmente não terá duplicatas.

Fazendo uso de \textbf{Sequências} a quantidade possível de identificadores gerados por milissegundo no sistema pode aumentar além do que 64 bits permite, pois entre sequências não é necessário a garantia de unicidade de identificadores.

Um exemplo de uso desse conceito seria um sistema no qual seja necessário gerar identificadores únicos para referenciar as entidades usuário e produto. Poderiam ser usadas sequências diferentes para as entidades, já que só é necessário garantir a unicidade dos identificadores para uma única entidade, mas não para as duas.

\textbf{Schemas} foram introduzidos com propósito de resolver um pequeno problema criado com a introdução do conceito de \textbf{Sequências}. Como \textbf{Sequências} são somente \textit{strings} de caracteres passados como argumento para a criação de identificadores, é possível que dois usuários estejam usando a mesma sem saber, e assim disputem a capacidade de geração de identificadores de uma única sequência.

Para resolver isto, \textbf{Schemas} foram introduzidos de forma a desambiguar os casos nos quais múltiplas \textbf{Sequências} com o mesmo nome sejam necessárias. Estes funcionam como recipientes lógicos, em que duas \textbf{Sequências} com o mesmo nome, usadas em \textbf{Schemas} diferentes se comportaram como se tivessem nomes diferentes.

Com objetivo de fazer com que \textbf{Schemas} não tenham o mesmo problema que \textbf{Sequências}, em que duas pessoas possam estar usando a mesma sem saber, estas devem ser criadas antes de serem usadas e como parte do processo de criação é verificado se um \textbf{Schema} com o mesmo nome já existe e, caso exista, a criação irá falhar.

\section{Tecnologias Utilizadas}
\label{sec:tecusadas}

A linguagem de programação utilizada para implementar este projeto foi Java EE 8 (Java Enterprise Edition 8), devido ao seu desempenho ajudar na latência e \textit{throughput} da geração de identificadores  e por seu ecossistema oferecer diversas bibliotecas e \textit{frameworks} necessários para o desenvolvimento desta aplicação. Dentre os mais importantes estão: Spring Boot, Netty, Protocol Buffers, ZooKeeper, Apache Curator e Dropwizard Metrics.

Spring Boot é um \textit{framework} que auxilia na criação de uma aplicação autônoma baseada em Spring, que toma uma posição mais opinativa e torna o desenvolvimento mais rápido. O uso deste \textit{framework} se deve à necessidade do uso do padrão de projeto injeção de dependência, para unir os componentes desenvolvidos,  ter a capacidade de gerenciar o ciclo de vida destes componentes, permitir a externalização das configurações de forma padronizada, etc. 

Netty é um \textit{framework} de I/O não bloqueante que permite o desenvolvimento rápido e fácil de aplicações de rede que seguem a arquitetura de cliente e servidor. Este foi utilizado tanto no desenvolvimento da biblioteca cliente quanto no servidor para que a comunicação entre eles pudesse ser realizada utilizando o protocolo TCP.

Protocol Buffers é um projeto desenvolvido pelo Google com o propósito de serializar dados estruturados de maneira extensível e neutro quanto a linguagens de programação e plataformas, gerando dados serializados compactos e de forma rápida. Neste projeto esta tecnologia foi usada para serializar as requisições e respostas trocadas pela biblioteca cliente e o servidor.

ZooKeeper é um serviço centralizado que provê coordenação distribuída de forma altamente confiável. Este essencialmente oferece um armazenamento de chave-valor hierárquico, que pode ser usado como um serviço de sincronização em um sistema distribuído, por exemplo. É um projeto \textit{open source}, maduro e já usado em diversos projetos conhecidos, como Kafka e Hadoop. Por essas razões, ele foi escolhido para resolver o problema de coordenação distribuída deste projeto, ao invés de seus alternativas ou mesmo uma implementação de consenso realizada como parte do sistema desenvolvido.  

Apache Curator é uma biblioteca cliente para ZooKeeper, que inclui diversas APIs de alto nível e utilidades para tornar a interação com ZooKeeper mais simples e confiável. O uso desta biblioteca foi necessária pois ZooKeeper foi utilizado para realizar coordenação entre as instâncias do \textit{cluster}, como citado anteriormente.

Google Guava é um conjunto de bibliotecas \textit{open source} Java que possuem novos tipos de classes de coleção, utilitários para lidar com concorrência, \textit{strings}, \textit{hashing}, etc.
Neste projeto foi utilizado o EventBus, fornecido pelo Guava, com o propósito de realizar a comunicação assíncrona que ocorre entre os diferentes componentes do sistema.

Dropwizard Metrics é uma biblioteca que provê instrumentação para medir e publicar o estado de uma aplicação Java. Esta biblioteca oferece um conjunto de instrumentos de medida que facilitam, por exemplo, a medição da taxa de um certo evento nos últimos 15 minutos, além disso ela também fornece formas de acessar estas medições em tempo real através de JMX, HTTP, etc.

\section{Arquitetura Física}
\label{sec:arq-fis}

Nesta seção é apresentada a arquitetura física deste sistema, projetada com objetivo de prover todas as funcionalidades enquanto respeitando todos os requisitos não-funcionais levantados e discutidos na Seção~\ref{sec:req-nfunc}. Um diagrama mostrando uma visão global da arquitetura física pode ser vista na Figura~\ref{fig-diagrama-arq-fisica}.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/arquitetura-fisica} 
	\caption{Diagrama da Arquitetura Física do Sistema.}
	\label{fig-diagrama-arq-fisica}
\end{figure}

A fim de cumprir os requisitos de escalabilidade e alta disponibilidade foi decidido que seria necessário que múltiplos servidores Sandgrain funcionassem como um \textit{cluster}. Desta forma caso algum dos servidores tivesse problemas, outros poderiam assumir o seu trabalho, assim fornecendo alta disponibilidade ao sistema.

Com este \textit{cluster} tendo a capacidade de receber novas instâncias a qualquer momento, que é a forma com que as instâncias foram projetadas neste projeto, o requisito de escalabilidade, como descrito na Subseção~\ref{reqesca}, também pode ser cumprido.

O \textit{cluster} ZooKeeper como mostrado no diagrama da Figura~\ref{fig-diagrama-arq-fisica} é usado somente pelas instâncias do \textit{cluster} Sandgrain, com o intuito de ser comunicaram e, assim, chegarem em consenso, que é necessário em algumas tarefas. Isso é discutido em mais detalhes na Seção~\ref{arqserv}, que discute os componentes dos servidores.

O último componente desta arquitetura física é a aplicação que faz uso de uma biblioteca que a permita se comunicar com os servidores do \textit{cluster} Sandgrain, para que possa invocar a API oferecida pelo serviço de forma simples.

Inicialmente a comunicação iria ser implementada usando o protocolo HTTP, mas alguns testes iniciais foram feitos e estes mostraram que para atingir a latência ideal para este tipo de projeto seria necessário um protocolo mais leve e rápido, por isso no final a comunicação foi realizada utilizando o protocolo TCP.

\section{Arquitetura do Servidor}
\label{arqserv}

Nesta seção serão discutidos os componentes que formam a arquitetura do servidor, a responsabilidade individual de cada um, como estes se comunicam uns com os outros e por fim como as métricas de funcionamento do sistemas são medidas e expostas.

No diagrama a seguir é possível ter uma visão global de todos estes componentes e das dependências que estes possuem uns dos outros. As subseções a seguir discutem cada um dos componentes.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/package-diagram.png} 
	\caption{Diagrama de Pacotes do Sistema.}
	\label{fig-diagrama-pacotes}
\end{figure}

\subsection{Namespace Ledger}
Este componente é responsável por gerenciar os \textit{namespaces} que as instâncias do \textit{cluster} podem utilizar para geração de identificadores ao receber requisições. Cada nó participante do \textit{cluster} recebe um conjunto de \textit{namespaces} de modo que dois conjuntos não possuam interseção, para que não ocorra colisão de identificadores.

Quando uma instância é inicializada o método \textbf{addManager} da interface deste componente é invocado, o que irá iniciar um processo que redistribuirá os \textit{namespaces} que cada nó pode utilizar para gerar identificadores. 

Essa redistribuição ocorre de modo que cada instância possua um número de \textit{namespaces} mais próximos aos outros o possível. Como ocorrerá uma redistribuição o novo nó inicializado também irá receber \textit{namespaces}, para que possa começar a gerar identificadores.

Ao finalizar de forma normal a aplicação também invocará \textbf{removeManagers}, para que os \textit{namespaces} dos nós sejam redistribuídos, e como um nó está deixando o \textit{cluster} os outros iram repartir os \textit{namespaces} antes usados pelo nó que está de saída.

Quando um nó deixa o \textit{cluster} de forma inesperada, um dos nós que foi eleito como líder invocará o método \textbf{removeAllManagersExceptFor} para que os \textit{namespaces} antes usados pelo nó que saiu possam ser redistribuídos e assim utilizados pelos nós que continuam servindo requisições.

Através destes três métodos as instâncias mantém os seus \textit{namespaces} atribuídos a cada uma, sempre atualizados e repartidos de forma mais igual o possível. Isso é possível através de uma estrutura de dados que foi chamada de \textit{ledger} que fica nos servidores ZooKeeper e é manipulada pelos nós Sandgrain afim de chegar em um consenso sobre quais nós podem utilizar quais \textit{namespaces}.  

Por meio de eventos este componente comunica ao restante dos componentes do servidor os \textit{namespaces} que foram atribuídos ou desatribuídos a este servidor, para que o sistema possa gerar identificadores de forma correta a partir destas informações.

\subsection{Namespace Quarantine}
Quando uma instância tem \textit{namespaces} atribuídos a ela, esta não pode começar a fazer uso destes para gerar identificadores imediatamente. Isso se dá pelo fato que outros nós do \textit{cluster} ainda podem estar usando os \textit{namespaces} e, caso dois \textit{namespaces} iguais sejam usados ao mesmo tempo, identificadores repetidos poderiam ser gerados.

Para evitar que \textit{namespaces} não sejam utilizados até outras instâncias tenham parado de usá-los, este componente armazena os \textit{namespaces} atribuídos a uma instância até que outras instâncias confirmem que pararam de fazer uso destes. Quando isso acontece, este componente dispara eventos dizendo quais \textit{namespaces} podem ser usados de forma segura pela instância.

Este componente também faz uso de uma estrutura de dados armazenada no ZooKeeper chamada de \textit{quarantine} que mantém informação sobre quais instâncias ainda podem estar fazendo uso de quais \textit{namespaces}. Quando o componente recebe um evento informando que um \textit{namespace} não está mas em uso, a estrutura é atualizada e os nós são notificados desta atualização.

\subsection{Namespace Container}
Este componente tem como principais objetivos manter uma coleção dos \textit{namespaces} que podem ser usados de forma segura pela instância e oferecer uma interface que permita outros módulos acessarem os \textit{namespaces} armazenados, enquanto rastreia se um \textit{namespace} está sendo usado no momento.

Este rastreamento se faz necessário pois, ao ter um \textit{namespace} desatribuído de si, uma instância precisa parar de fazer uso deste \textit{namespace} e, ao parar, deve informar aos outros nós que fez isto. Como a instância só pode notificar os outros métodos quando não estiver mas fazendo uso do \textit{namespace}, é necessário rastrear o uso de \textit{namespaces} dentro de uma instância. 

O gerenciamento de \textit{namespaces} é realizado escutando eventos publicados pelos outros componentes do sistema e manipulando a coleção interna. Quando um evento, por exemplo, com informação que um \textit{namespace} pode sair com segurança da quarentena, o componente deve adicioná-lo à coleção interna.

Para oferecer acesso aos \textit{namespaces} armazenados, a interface do componente permite a criação de \textit{Pools} de \textit{namespaces}. Com o uso do padrão de projeto \textit{Pool} é possível que os usuários da interface usem os \textit{namespaces} e o componente consiga rastrear quais destes estão sendo usados no momento.

\subsection{Namespace Distribution}
Como os nós do \textit{cluster} podem parar de funcionar a qualquer momento, é necessário que, caso isto aconteça, outra instância possa realizar ou finalizar tarefas que um nó deveria ter realizado antes de deixar o \textit{cluster}.

A principal tarefa neste caso é a redistribuição de \textit{namespaces}, que deve ocorrer quando um nó deixa o \textit{cluster}. Caso essa redistribuição não ocorra entre os nós restantes, estes não poderiam fazer uso dos \textit{namespaces}, assim diminuindo a capacidade de geração de identificadores por milissegundo.

Para lidar com esse desafio, este componente utiliza o \textit{framework} Apache Curator e ZooKeeper para realizar eleições de nós líderes, de modo a sempre ter no máximo um nó líder em dado momento da execução do \textit{clutser}. Este nó líder tem como responsabilidade efetuar a redistribuição de \textit{namespaces} caso um nó deixe o \textit{cluster} sem fazê-lo.

\subsection{Id Generator}
Este componente foi criado com a responsabilidade de gerar os identificadores a serem retornados aos usuários e criar, remover e listar todos os \textit{schemas} do \textit{cluster}.

A partir dos métodos \textbf{create}, \textbf{delete} e \textbf{list} da interface \textbf{SchemaService}, oferecida por este componente, uma estrutura de dados armazenada no ZooKeeper é manipula a fim de gerenciar os \textit{schemas} que podem ser usados para gerações de identificadores em todo \textit{cluster}.

A geração de identificadores é provida, por este componente, pelo método \textbf{generateId} que, a partir de um \textit{schema} existente no \textit{cluster}, o nome de uma \textit{sequence} e a quantidade desejada de identificadores a serem gerados, gera e retorna os identificadores.

\subsection{Identity Access}
Este componente tem como funcionalidades o gerenciamento de regras de acesso a recursos do \textit{clutser}, como \textit{schemas}, e a autorização de operações feitas por usuários sobre estes recursos.

Através dos métodos \textbf{createAclRule}, \textbf{deleteAclRule} e \textbf{describe}, o componente oferece a capacidade de gerenciar o acesso de usuários a certos recursos do \textit{cluster}. Os métodos \textbf{createAclRule} e \textbf{deleteAclRule} permitem a criação e remoção de regras de acesso a recursos, respectivamente, enquanto \textbf{describe} provê uma forma de ler todas as regras de acesso criadas.

A persistência destas regras criadas pelos usuários é feita por meio de uma estrutura de dados simples que é mantida nos servidores ZooKeeper.

A autorização é desempenhada através do uso do método \textbf{checkGranted} que é capaz de verificar se, dadas as regras de acesso registradas no sistema, é permitido ou não a um usuário realizar uma dada operação sobre um recurso especificado. A autenticação de usuários necessária para realizar a verificação de autorização é discutida na Seção~\ref{sec:nettyserver}.

\subsection{Health Report}
Este componente oferece, por meio de sua interface, um relatório sobre a saúde da instância. Ao invocar o método \textbf{getReport}, diferentes informações sobre a capacidade da instância de realizar suas funcionalidades como se é esperado por seus usuários são retornadas.

Essas informações podem ser usadas por clientes para determinar se certas instâncias poderão responder às suas requisições ou se será necessário parar de rotear requisições para esta instância temporariamente ou de forma permanente.

Essas informações também são importantes para futuras ferramentas que monitorem o \textit{cluster}, para que seus operadores saibam se algum problema está acontecendo e se é necessária a sua intervenção.

\subsection{Service Discovery}
O objetivo deste componente é fazer com que os clientes do \textit{cluster} Sandgrain consigam, ao se conectar, saber quais nós existem e ter informações para que possam se conectar a eles. Três métodos fazem parte da interface deste componente: \textbf{registerService}, \textbf{unregisterService} e \textbf{getOnlineSandgrainHosts}.

Quando a instância foi inicializada completamente e está pronta para servir requisições, o método \textbf{registerService} é invocado e este modificará uma estrutura de dados no ZooKeeper, inserindo as informações necessárias para que clientes consigam se conectar ao nó. Ao modificar esta estrutura de dados todos os nós do \textit{cluster} serão notificados e também receberão as informações inseridas pelo nó ingressante.

O método \textbf{unregisterService} será invocado quando a instância inciar o seu processo de desligamento, assim notificando os outros nós e clientes do \textit{cluster} que ele não estará mais respondendo requisições. Este método modifica a mesma estrutura de dados que \textbf{registerService}, desta vez removendo as informações antes adicionadas pela mesma instância.

O método \textbf{getOnlineSandgrainHosts} retorna as informações registradas pelos nós através de \textbf{registerService}. Já \textbf{getOnlineSandgrainHosts} é utilizada atualmente pelos clientes do \textit{cluster} para que possam, a partir da conexão com um nó, ter informação suficiente para se conectar com os demais.

\subsection{Netty Server}
\label{sec:nettyserver}

Este componente tem como responsabilidade receber as requisições, enviadas pelo cliente através da rede e, baseado na requisição, invocar operações implementadas nos outros componentes. Além disso, também é o dever deste componente realizar o gerenciamento do ciclo de vida dos outros componentes do servidor.

Para implementar as funcionalidades deste componente foi feito uso extensivo dos \textit{frameworks} Netty e Spring Boot. Netty foi utilizado para implementar todos os aspectos da comunicação que o servidor faz com o cliente, enquanto Spring Boot foi usado para implementar a externalização das configuração do servidor, instanciar todos os componentes e gerenciar o ciclo de vida destes.

Seguindo o padrão de projeto MVC, para cada operação que usuários podem realizar através de requisições, existe uma classe que irá receber a requisição, delegar a realização da operação para outro módulo, receber o resultado desta operação e retornar uma resposta. 

A serialização e desserialização destas requisições e respostas é efetuada usando Protocol Buffers.
Compartilhando uma especificação entre a biblioteca cliente e o servidor, estes conseguem interpretar as mensagens enviadas entre eles. A partir desta especificação uma série classes Java são compiladas e proveem as estruturas das requisições e respostas definidas na especificação e os mecanismos capazes de realizar a serialização e desserialização destas.

Também utilizando Netty foi possível implementar criptografia para a comunicação entre clientes e servidores, verificação de integridades dos dados enviados e autenticação de clientes. Todas estas funcionalidades proveem do uso do protocolo de segurança SSL, implementado por este componente com suporte da classe \textbf{SslHandler}, provida por Netty.

\subsection{Métricas}
Em todos os componentes citados nas subseções anteriores, métricas são medidas e capturadas para aumentar a capacidade dos operadores do \textit{cluster} de servidores Sandgrain detectarem qualquer anomalia e poderem tomarem decisões sobre o que deve ser feito para voltar este \textit{cluster} para um estado normal.

Usando as estruturas de medição fornecidas pela biblioteca Dropwizard Metrics cada componente mede a ocorrência de eventos importantes em seu contexto usando a estrutura de medida que faz mais sentido para cada tipo de evento. Um exemplo seria a métrica \textbf{idGenerationThroughput.meter}, que mede quantos identificadores foram gerados por segundo nos últimos 5,10 e 15 minutos.

Com as medidas sendo feitas de forma correta, a última coisa a fazer com as métricas é expô-las aos usuários. Para isso foi usado um exportador JMX, fornecido por Dropwizard Metrics, que expõe as métricas, sem a necessidade de muita configuração, para serem consumidas por qualquer cliente JMX.

\section{Biblioteca Cliente}
\label{sec:bib-client}

Para que aplicações possam fazer uso das funcionalidades oferecidas pelo serviço desenvolvido, estas precisam se comunicar com o as instâncias do \textit{cluster}. A fim de facilitar a implementação da comunicação que deve ocorrer, foi desenvolvida uma biblioteca que pode ser usada por aplicações clientes para invocar as funcionalidades desejadas no serviço.

A biblioteca é responsável somente por gerenciar a conexão com o \textit{cluster} de forma transparente e realizar as requisições ao serviço. Ela foi mantida o mais simples possível para que novas implementações possam ser criadas, sem muito trabalho e, assim, mais projetos possam ter acesso ao serviço desenvolvido neste trabalho.

Como parte do gerenciamento da conexão com o \textit{cluster}, a biblioteca implementa o descobrimento das instâncias e a criação e manutenção das conexões feitas com estas instâncias. 

O descobrimento é um processo executado pelo cliente continuamente para que este possa saber quais instâncias estão conectadas ao \textit{cluster} a todo momento e assim possa se conectar e realizar as requisições desejadas.

Tendo estas informações, o cliente se conecta a múltiplas instâncias e supervisiona estas conexões para que, caso algum problema ocorra, este possa simplesmente criar uma nova conexão com outro nó que não esteja apresentando problemas e continuar realizando suas atividades.

Com as conexões estabelecidas, a API oferecida pela biblioteca pode ser usada para realizar as operações sobre o serviço, como gerar identificadores, sendo que cada método oferecido pela API resulta na criação de uma requisição, que é serializada, enviada a uma das instâncias do serviço e recebe uma resposta.

Para interoperar de forma fácil com as instâncias, o cliente também utiliza o projeto Protocol Buffers para criação, serialização e deserialização de requisições e respostas trocadas pelos dois, como já citado na Seção~\ref{sec:nettyserver}. Compartilhando a estrutura das mensagens que são trocadas entre clientes e nós, a implementação da interoperabilidade se tornou simples.   
