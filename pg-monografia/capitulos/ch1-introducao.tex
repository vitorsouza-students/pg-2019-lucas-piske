% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 1 - Introdução
% ==============================================================================
\chapter{Introdução}
\label{sec-intro}

Com o advento da Internet puderam ser desenvolvidos sistemas capazes de serem usados por usuários independente da sua localização, desde que estes possuam um navegador. Com o passar do tempo o número de experiências que usuários podem ter usando a Internet aumentou e a qualidade destas experiências também melhoraram, fazendo assim a Internet mais útil para cada vez um grupo maior de pessoas. Em maio de 2019 a porcentagem de pessoas que possuem acesso a Internet mundialmente era de 56,8\%~\cite{internetworldstats}.

Assim, os sistemas que usam a Internet como meio de interação puderam ter uma base de usuários muito maior do que se era comum antes dela. Um exemplo é a rede social Facebook, que em abril de 2019 reportou possuir 2,38 bilhões usuários ativos mensais~\cite{zephoria-fb-mau}.

Com aplicações que possuem quantidades de usuários grandes, como a que foi citada anteriormente, se tornando possíveis, novos desafios surgiram e outros que não eram muito comuns começaram a afligir mais projetos do que no passado. Um exemplo de desafio é gerenciar os grandes volumes de dados que são produzidos atualmente. Para lidar com esse problema novas tecnologias vem sendo desenvolvidas, como bancos de dados NoSQL, que são desenvolvidos em sua maioria com esse problema em mente. 

O desafio que é o foco deste trabalho é a geração de identificadores de 64-bits para situações que requerem um \textit{throughput} de geração alto. Identificadores tem muitas utilidades, uma das principais em sistemas de informação é a capacidade de anexá-los a um registro do banco de dados, quando inserido, para que depois este registro possa ser referenciado de forma única entre todos os registros contidos em um conjunto, o tipo de atributo que possui esta característica em banco de dados relacionais geralmente é chamado de chave primária.

Alguns tipos de registros possuem atributos que naturalmente serviriam como chave primária, um exemplo seria um atributo que contém um CPF num registro que representa uma pessoa física. Mas outros tipos de registros não possuem um atributo que os identifica de forma natural, mesmo que estes precisem serem identificados de forma única. Para solucionar este problema são gerados identificadores únicos seguindo alguma estratégia e um identificador gerado é adicionado como um atributo extra ao registro e este atributo servirá como sua chave primária.

Neste trabalho é apresentado o processo de desenvolvimento de um sistema que implementa uma estratégia de geração de identificadores capaz de gerar identificadores numéricos de 64-bits e dá suporte a casos que tenham como requisito um \textit{throughput} de geração de identificadores muito alto. Esse desafio já foi identificado por outras organizações~\cite{insta-eng-blog-ids} e diferentes soluções já foram propostas, mas nenhuma implementação pode ser facilmente implantada em novos ambientes como é comum ser feito com banco de dados de prateleira (MySQL, PostgreSQL e etc), por exemplo. 

O sistema proposto é um serviço de rede, que futuramente será publicado de forma a ser \textit{open source}. Esse serviço funcionará de forma distribuída e terá a capacidade de escalar linearmente horizontalmente, ou seja o sistema deverá ser capaz de receber novos nós ao \textit{cluster} e ao receber um novo nó a sua capacidade de gerar identificadores por segundo deve aumentar de forma linear.

\section{Objetivos}
\label{sec-objetivos}

Este trabalho tem como objetivo o desenvolvimento de um sistema distribuído linearmente escalável capaz de gerar \textit{surrogates} numéricos de 64-bits, fazendo uso do conhecimento adquirido durante o curso de Ciência da Computação. São objetivos específicos deste projeto:

\begin{itemize}
	\item Levantamento e documentação dos requisitos, de forma a levar em conta as possíveis necessidades
	da maioria de usuários em potencial, assim reduzindo a probabilidade de novas soluções semelhantes
	serem necessárias no futuro;
	
	\item Uso de estruturas e arquiteturas do sistema que atendam as necessidades
	do usuário descobertas durante o levantamento de requisitos e formem uma fundação sólida que 
	aceite evoluções futuras do software;
	
	\item Implementação do sistema seguindo as estruturas e arquiteturas definidas de forma precisa para que se tenha certeza que o código sendo desenvolvido esteja alinhado com as necessidades do usuário final;
	
	\item Realização de testes de performance para que o comportamento do sistema em diferentes
	cenários possa ser conhecido e essa informação possa ser usada por futuros usuários quando estes implantarem o sistema em seus ambientes;
	
	\item Aplicação do conhecimento obtido durante o Curso de Computação, em particular nas disciplinas de Sistemas Distribuídos, Banco de Dados, Programação III, Engenharia de Software e Projeto de Sistemas de Software.
\end{itemize}

\section{Metodologia}
\label{sec-metodo}

Para atingir os objetivos apresentados na seção anterior, os seguintes passos foram realizados:

\begin{enumerate}
	\item Revisão Bibliográfica: pesquisar o conteúdo necessário para realizar os objetivos do projeto. Isso inclui estudar temas de Sistemas Distribuídos, Redes, entre outros. Além disso também pesquisar possíveis ferramentas, bibliotecas e \textit{frameworks} que podem auxiliar na fase de implementação;
	
	\item Design do Sistema: levantar e analisar de maneira aprofundada todos os requisitos das funcionalidades do sistemas, bem como definir as tecnologias e arquiteturas a serem utilizadas no desenvolvimento;
	
	\item Estudo das Tecnologias: estudar a fundo as tecnologias pesquisadas durante a revisão bibliográfica. Alguns exemplos destas tecnologias são: Apache ZooKeeper, Apache Curator, Netty, Protocol Buffers, entre outras;
	
	\item Implementação e Testes Funcionais: realizar a implementação do sistema seguindo as descrições da fase de projeto e, para cada grupo de funcionalidades implementado, efetuar testes para avaliar a exatidão do código;
	
	\item Testes de Desempenho: realizar testes para avaliar o desempenho do sistema desenvolvido em diferentes cenários;
	
	\item Redação da Monografia: escrever a monografia usando a linguagem de marcação \textit{LaTeX}\footnote{\url{https://www.latex-project.org/}.} com o editor \textit{TexStudio}\footnote{\url{https://www.texstudio.org/}.} a partir do \textit{template} \textit{abnTeX},\footnote{\url{https://www.abntex.net.br/}.} que segue as normas impostas pela ABNT (Associação Brasileira de Normas Técnicas) para documentos técnicos e científicos brasileiros.
\end{enumerate}

\section{Organização da Monografia}
\label{sec-org-mono}

Além desta introdução, esta monografia é composta por outros cinco capítulos.

Capitulo~\ref{sec-referencial}: é realizada uma discussão sobre os diversos fundamentos teóricos usados para desenvolver este trabalho. Sendo estes fundamentos: Engenharia de Software, Especificação e Análise de Requisitos, Projeto de Sistemas e Implementação, Testes de Desempenho e Sistemas Distribuídos.

Capitulo~\ref{sec-requisitos}: este capítulo contém a descrição do escopo do problema que este projeto se propõe a resolver e a especificação dos requisitos funcionais e não-funcionais do sistema.

Capitulo~\ref{sec-projeto}: descreve a solução do problema para geração de identificadores de forma distribuída e os componentes relevantes da arquitetura do sistema desenvolvido.

Capitulo~\ref{cap-tests}: explica os testes funcionais executados e o processo realizado para efetuar os testes de desempenho, descreve o ambiente usado e apresenta seus resultados.

Capitulo~\ref{sec-conclusoes}: apresenta a conclusão do trabalho, dificuldades
encontradas, limitações e propostas de trabalhos futuros.