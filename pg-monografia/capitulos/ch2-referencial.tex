% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}
Este capítulo apresenta os principais conceitos teóricos que fundamentaram o
desenvolvimento deste projeto e trabalhos relacionados com este. Este capítulo está organizado em 7 seções, sendo estas:
Engenharia de Software (\ref{sec:ref-eng-soft}), Especificação e Análise de Requisitos (\ref{sec:ref-req}), Projeto de Sistemas e Implementação (\ref{sec:ref-proj}), Testes Funcionais (\ref{sec:ref-test-func}), Testes de Desempenho (\ref{sec:ref-test-des}), Sistemas Distribuídos (\ref{sec:ref-sis-dis}) e Trabalhos Relacionados (\ref{sec:trab-rel}).

\section{Engenharia de Software}
\label{sec:ref-eng-soft}
Infraestruturas e serviços nacionais são controlados por sistemas computacionais e a maioria dos produtos elétricos inclui um computador e um software que o controla. A manufatura e a distribuição industriais são totalmente informatizadas, assim como o sistema financeiro. Por este motivo a Engenharia de Software se tornou uma área da computação tão importante para praticamente toda a sociedade moderna~\cite{engsoft}. 

A Engenharia de Software surgiu visando melhorar a produtividade do processo de desenvolvimento e, ao mesmo tempo, a qualidade dos produtos de software \cite{barcellos}. A Engenharia de Software auxilia na produção de software provendo ferramentas de apoio para as atividades, métodos para orientar a realização das atividades, processo para definir as atividades e os produtos e a qualidade de processo e de produto de software~\cite{kechi}.

Ao longo da história da Engenharia de Software foram sendo construídas ferramentas computadorizadas para apoiar o desenvolvimento. Essas iniciativas avançaram bastante, mas ainda assim necessitam da intervenção humana. Foram concebidos vários modelos de processos de software e nenhum pode ser considerado o ideal, devido às suas divergências. Entretanto, todos compartilham de atividades fundamentais como especificação, projeto e implementação, validação e evolução~\cite{engsoft}.

\section{Especificação e Análise de Requisitos}
\label{sec:ref-req}

O termo `requisito' não é usado de forma consistente pela indústria de software. Em alguns casos, o requisito é apenas
uma declaração abstrata em alto nível de um serviço que o sistema deve oferecer ou uma restrição a um sistema. No outro
extremo, é uma definição detalhada e formal de uma função do sistema~\cite{engsoft}.

Os requisitos de um sistema são as descrições do que o sistema deve fazer, os serviços que oferece e as restrições a seu funcionamento. Esses requisitos refletem as necessidades dos clientes para um sistema que serve a uma finalidade determinada, como controlar um dispositivo, colocar um pedido ou encontrar informações~\cite{engsoft}.

O levantamento de requisitos (também chamado elicitação de requisitos) combina elementos de resolução de problemas, elaboração, negociação e especificação. Para encorajar uma abordagem colaborativa e orientada às equipes em relação ao levantamento de requisitos, os interessados trabalham juntos para identificar o problema, propor elementos da solução, negociar diferentes abordagens e especificar um conjunto preliminar de requisitos da solução~\cite{pressman}.

Há várias técnicas utilizadas para isso, como, por exemplo: leitura de obras de referência e livros-texto, observação do ambiente do usuário, realização de entrevistas com os usuários, entrevistas com especialistas do domínio, reutilização de análises anteriores, comparação com sistemas preexistentes do mesmo domínio do negócio. O produto do levantamento de requisitos é o documento de requisitos, que declara os diversos tipos de requisitos do sistema~\cite{bezerra}. As seções essenciais deste documento são:

\begin{enumerate}
	\item Requisitos funcionais: definem funcionalidades oferecidas pelo sistema. Um exemplo seria oferecer o horário atual aos usuários;
	\item Requisitos não-funcionais: estabelecem restrições de qualidade para a implementação do sistema. Por exemplo: usabilidade e segurança;
	\item Requisitos normativos: são restrições impostas sobre o processo de desenvolvimento de software. Como prazos, por exemplo.
\end{enumerate}

No contexto dos sistemas de software, a etapa de análise de requisitos é a etapa em que os analistas realizam um estudo detalhado dos requisitos levantados na atividade anterior. A partir desse estudo, são construídos modelos para representar o sistema a ser construído~\cite{bezerra}.

De maneira simples, um modelo é uma simplificação da realidade enfocando certos aspectos considerados relevantes, segundo a perspectiva do modelo, e omitindo os demais. Modelos são construídos para se obter uma melhor compreensão da porção da realidade sendo
modelada~\cite{barcellos}.

Estes modelos produzidos são essenciais para o processo de desenvolvimento de sistemas. Estes geralmente são criados para enfocar os aspectos chave, possibilitar o estudo do comportamento do sistema, facilitar a comunicação entre membros da equipe de desenvolvimento e clientes e
usuários, possibilitar a discussão de correções e modificações com o usuário, servir como base para a tomada de decisão e formar a documentação do sistema~\cite{barcellos}.

\section{Projeto de Sistemas e Implementação}
\label{sec:ref-proj}

Após a Especificação de Requisitos de Software, complementada por
diagramas e protótipos da atividade de Análise, a atividade de Projeto
representa a ligação entre a codificação do software e os seus requisitos. A
atividade de Projeto visa estabelecer uma arquitetura do software que seja
realizável, apoiada em definições de módulos funcionais, detalhamento dos
procedimentos ou algoritmos e interfaces entre os módulos~\cite{kechi}.

Inicialmente, o projeto é representado em um nível alto de abstração, enfocando
a estrutura geral do sistema. Definida a arquitetura, o projeto passa a tratar do detalhamento de seus elementos. Esses refinamentos conduzem a representações de menores
níveis de abstração, até se chegar ao projeto de algoritmos e estruturas de dados. Assim,
independentemente do paradigma adotado, o processo de projeto envolve as seguintes
atividades~\cite{falbo}:

\begin{enumerate}
	\item Projeto da Arquitetura do Software: determina as estruturas arquiteturais do sistema e
	seus relacionamentos;
	\item Projeto dos Elementos da Arquitetura: o design de cada estrutura arquitetural definidas é realizado em mais detalhes, o que requer subdividir essas estruturas em outras estruturas menores;
	\item Projeto Detalhado: entra em mais detalhes sobre os elementos fundamentais da arquitetura, como por exemplo, interfaces. Também estabelece como os componentes de software se comunicarão, como este sistema se integrará como outros sistemas, como pessoas irão operar o software, além de projetar detalhes de algoritmos e estruturas de dados.
\end{enumerate}

Um projeto trata de como resolver um problema, por isso, sempre existe
um processo de projeto. No entanto, nem sempre é necessário ou apropriado descrever o projeto em detalhes usando a
UML ou outra linguagem de descrição~\cite{engsoft}.

A atividade de Implementação tem por objetivo traduzir as especificações
de software em códigos de programa que possam ser processados por um
sistema computacional. Naturalmente, esses códigos são dependentes da
linguagem de programação escolhida. As características da linguagem
e o estilo de codificação podem afetar profundamente a qualidade e a
manutenibilidade do software~\cite{kechi}.

As atividades de projeto e implementação de software são invariavelmente intercaladas. O projeto de software é uma
atividade criativa em que você identifica os componentes de software e seus relacionamentos com base nos requisitos do
cliente. A implementação é o processo de concretização do projeto como um programador ou esboçado em um quadro ou em papel~\cite{engsoft}.

\section{Testes Funcionais}
\label{sec:ref-test-func}
Os testes funcionais, ou de caixa-preta, utilizam as especificações (de requisitos, análise
e projeto) para definir os objetivos do teste e, portanto, para guiar o projeto de casos de teste.
Os testes são conduzidos na interface do software e o programa ou sistema é considerado uma
caixa-preta. Para testar um módulo, programa ou sistema, são fornecidas entradas e avaliadas
as saídas geradas para verificar se estão em conformidade com a correspondente especificação~\cite{falbo}.

Os testes caixa-preta são empregados para demonstrar que as funções do software estão
operacionais, que a entrada é adequadamente aceita e a saída é corretamente produzida, e que
a integridade da informação externa (uma base de dados, por exemplo) é mantida~\cite{falbo}.

Em princípio, o teste funcional pode detectar todos os defeitos, submetendo o programa
ou sistema a todas as possíveis entradas, o que é denominado teste exaustivo. No
entanto, o domínio de entrada pode ser infinito ou muito grande, de modo a tornar o tempo
da atividade de teste inviável, fazendo com que essa alternativa se torne impraticável~\cite{delamaro}.

Essa limitação da atividade de teste, que não permite afirmar, em geral, que o programa esteja
correto, fez com que fossem definidas as técnicas de teste e os diversos critérios pertencentes
a cada uma delas. Assim, é possível conduzir essa atividade de maneira mais sistemática,
podendo-se inclusive, dependendo do critério utilizado, ter uma avaliação quantitativa da
atividade de teste~\cite{delamaro}.

\section{Testes de Desempenho}
\label{sec:ref-test-des}

Uma vez que o sistema tenha sido totalmente integrado, é possível testá-lo para propriedades emergentes, como desempenho e confiabilidade. Os testes de desempenho precisam ser projetados para assegurar que o sistema possa processar a carga a que se destina. Isso normalmente envolve a execução de uma série de testes em que você aumenta a carga até que o desempenho do sistema se torne inaceitável~\cite{engsoft}.

O teste de desempenho é feito em todas as etapas no processo de teste. Até mesmo em nível de unidade, o desempenho de um módulo individual pode ser avaliado durante o teste. No entanto, o verdadeiro desempenho de um sistema só pode ser avaliado depois que todos os elementos do sistema  estiveram totalmente integrados~\cite{pressman}.

Os testes de desempenho muitas vezes são acoplados ao teste de esforço e usualmente requerem instrumentação de hardware e software. Isto é, frequentemente é necessário medir a utilização de recursos (por exemplo, ciclos de processador) de forma precisa. Instrumentação externa pode monitorar intervalos de execução, log de eventos (por exemplos, interrupções) à medida que ocorrem, e verificar os estados da máquina regularmente. Monitorando o sistema com instrumentos, o testador pode descobrir situações que levam à degradação e possível falha do sistema~\cite{pressman}.

\section{Sistemas Distribuídos}
\label{sec:ref-sis-dis}

Nos dias de hoje, muitas aplicações precisam estar funcionando 24 horas por dia, todos os dias do ano, e possuem requisitos de disponibilidade e confiabilidade que antes eram necessários em somente um pequeno conjunto de serviços críticos existentes no mundo. Da mesma forma, o potencial de crescimento rápido também significa que aplicações devem ser construídas para que possam escalar de forma quase instantânea em resposta à demanda de usuários. Essas necessidades fazem com que grande parte das aplicações construídas nos dias de hoje precisem ser um sistema distribuído~\cite{burns}.

Várias definições de sistemas distribuídos já foram dadas na literatura, nenhuma delas satisfatória e de acordo com nenhuma das outras~\cite{tanenbaum}. Para a finalidade deste projeto foi suficiente dar uma caracterização sem ser muito específica:

\begin{displayquote}
	\textit{Um sistema distribuído é uma coleção de computadores independentes que aparentam ser um único sistema coerente para seus usuários.}
\end{displayquote}

Essa definição tem vários aspectos importantes. O primeiro é que um sistema distribuído consiste em componentes (isto é, computadores) autônomos. Um segundo aspecto é que os usuários, sejam pessoas ou programas, acham que estão tratando com um único sistema. Isso significa que, de um modo ou de outro, os componentes autônomos precisam colaborar~\cite{tanenbaum}.

Os desafios advindos da construção de sistemas distribuídos são a heterogeneidade dos componentes, ser um sistema aberto, o que permite que componentes sejam adicionados ou substituídos, a segurança, a escalabilidade – isto é, a capacidade de funcionar bem quando a carga ou o número de usuários aumenta –, o tratamento de falhas, a concorrência de componentes, a transparência e o fornecimento de um serviço de qualidade~\cite{coulouris}.

\section{Trabalhos Relacionados}
\label{sec:trab-rel}

Durante a realização deste trabalho foram analisadas múltiplos projetos que se propõem a resolver o desafio aqui apresentado. Tais trabalhos estudados foram desenvolvidos pelos seguintes serviços \textit{web}: Flickr, Instagram e Twitter. Como todos os trabalhos foram desenvolvidos internamente por estas empresas, a informação sobre o seu desenvolvimento é limitada e somente foi possível ter acesso ao que as empresas compartilharam publicamente. Nas 3 seguintes subseções são discutidos os trabalhos por empresa e seus problemas identificados, que o projeto discutido nesta monografia busca resolver.

\subsection{Flickr}
O Flickr utiliza as capacidades ``auto increment'' do sistema de gerência de banco de dados MySQL. A empresa cria uma tabela, com um campo que possui o atributo \textit{auto\_increment}, em duas instâncias de banco de dados MySQL diferentes e esta tabela é usada para criar uma sequência de números únicos que servem como identificadores.

Em ambas as tabelas o campo é incrementado em dois a cada geração de identificador, mas em uma o seu valor inicial é 1 e na outra dois, assim uma instância MySQL gerará uma sequência de números pares e outra uma sequência de números impares. Dessa forma, as duas instâncias podem gerar identificadores de forma concorrente sem a ameaça de gerar dois identificadores iguais.

Um problema com esta estratégia é que não poderia ser usada com mais máquinas caso seja necessário uma capacidade maior de geração de identificadores, podendo se tornar um gargalo para operações que precisem de gerar identificadores. Outro problema é a necessidade de um banco de dados, que na sua implementação tem várias funcionalidades que não seriam usadas e geram um \textit{overhead} para geração de identificadores, aumentando a latência das requisições.

\subsection{Twitter}
\label{subsec-twitter}
Para gerar identificadores 64-bits de uma forma não coordenada, o Twitter criou uma aplicação de rede que produz identificadores compondo os bits de seus identificadores usando uma combinação de um número sequencial, um número único para o servidor que está rodando a aplicação geradora de identificadores e o \textit{timestamp} do momento que o identificador foi gerado.

O número sequencial é atribuído às \textit{threads} dentro da aplicação que gera identificadores e nenhuma \textit{thread} recebe os mesmo números. O número único do servidor é escolhido usando Zookeeper de forma que nenhum servidor escolha o mesmo número.

O problema dessa solução é que sua implementação depende altamente da infraestrutura criada internamente pelo Twitter e dessa forma não pode ser facilmente usada por outras organizações.

\subsection{Instagram}
O Instagram possui uma arquitetura na qual seus dados são repartidos em múltiplas instâncias de banco de dados, sendo que cada instância contém um conjunto de dados que não possui interseção com as outras instâncias. 

Para realizar a geração dos identificadores uma função escrita usando a linguagem PG/PGSQL é adicionada a todas as instâncias quando estas são implantadas. Com esta função instalada, ela pode ser invocada dentro da instância, por exemplo quando um registro está sendo inserido em uma tabela.

A função escrita retorna um identificador de 64-bits que é composto por um \textit{timestamp} do momento em que o identificador foi gerado, um identificador que é único na instância de banco de dados que está executando a função e um número sequencial que é incrementado a cada geração dentro de um milissegundo, permitindo gerar múltiplos identificadores por milissegundo por instância de banco de dados.

Essa implementação se mostra dependente na infraestrutura já existente internamente na organização assim como a solução discutida na Seção~\ref{subsec-twitter}. Além disso, da forma que o identificador é composto esta solução só permite a geração de 1024 identificadores por milissegundo em cada instância.